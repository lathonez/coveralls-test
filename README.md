# Elliptic aml-utils

[![Coverage Status](https://coveralls.io/repos/bitbucket/lathonez/coveralls-test/badge.svg?branch=master)](https://coveralls.io/bitbucket/lathonez/coveralls-test?branch=master)

This is a library that contains a set of utilities used by aml-api. In particular:

- Custom errors used when returning a response from aml-api, e.g. BadRequest, Forbidden etc.
- Formatting functions (sql escaping)
- Validator functions, also used by swagger node middleware. E.g. Bitcoin and Ethereum addresses validators
- Express middlewares to validate params types
