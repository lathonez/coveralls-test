const T = require('./types');

const ensureType = (type) => (req, res, next, id, name) => {
  let error;
  try {
    T.ensureType(type, id, `Invalid ${name}`);
    return next();
  } catch (error1) {
    error = error1;
    return next(error);
  }
};

const ensureUUID = ensureType('UUID');

const ensureHex32 = ensureType('Hex32');

const ensureInteger = ensureType('IntString');

module.exports = {
  ensureType,
  ensureUUID,
  ensureHex32,
  ensureInteger,
};
