const create = require('create-error');

const AppError = create('AppError');

const RequestError = create(AppError, 'RequestError');

RequestError.toJSON = () => ({
  message: this.message,
});

const Forbidden = create(RequestError, 'ForbiddenError', {
  status: 403,
});

const Unauthorized = create(RequestError, 'UnauthorizedError', {
  status: 401,
});

const BadRequest = create(RequestError, 'BadRequestError', {
  status: 400,
});

const NotFound = create(RequestError, 'NotFoundError', {
  status: 404,
});

const ConflictError = create(RequestError, 'ConflictError', {
  status: 409,
});

const ServerError = create(RequestError, 'ServerError', {
  status: 500,
});

const InvalidArguments = create(RequestError, 'InvalidArgumentsError', {
  status: 500,
});

const ServerTimeout = create(RequestError, 'ServerTimeoutError', {
  status: 503,
});

module.exports = {
  RequestError,
  ServerError,
  Forbidden,
  Unauthorized,
  BadRequest,
  NotFound,
  ConflictError,
  InvalidArguments,
  ServerTimeout,
};
