const _ = require('lodash');

const includeNested = (orm, models) => _.map(_.toPairs(models), (arg) => {
  const [modelName, inclusion] = arg;
  const include = includeNested(orm, inclusion);
  const model = orm[modelName];
  if (_.isEmpty(include)) {
    return model;
  }
  return {
    model,
    include,
  };
});

module.exports = {
  includeNested,
};
