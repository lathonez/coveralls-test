const TC = require('type-check');

const _ = require('lodash');

const E = require('./errors');

const V = require('./validations');

const customTypes = {
  Integer: {
    typeOf: 'Number',
    validate(x) {
      return x % 1 === 0;
    },
  },
  IntString: {
    typeOf: 'String',
    validate: V.isInt,
  },
  BoolString: {
    typeOf: 'String',
    validate: V.isBoolean,
  },
  Hex32: {
    typeOf: 'String',
    validate(x) {
      return V.isHexadecimal(x) && x.length === 32;
    },
  },
  Email: {
    typeOf: 'String',
    validate: V.isEmail,
  },
  UUID: {
    typeOf: 'String',
    validate: V.isUUID,
  },
  UUIDv4: {
    typeOf: 'String',
    validate(x) {
      return V.isUUID(x, 4);
    },
  },
  JSON: {
    typeOf: 'String',
    validate: V.isJSON,
  },
  Url: {
    typeOf: 'String',
    validate: V.isURL,
  },
  DateString: {
    typeOf: 'String',
    validate: V.isDate,
  },
  NonEmptyArray: {
    typeOf: 'Array',
    validate: V.nonEmpty,
  },
  NonNullString: {
    typeOf: 'String',
    validate: V.nonEmpty,
  },
  CustomerReference: {
    typeOf: 'String',
    validate: V.isCustomerReference,
  },
  CustomerLabelName: {
    typeOf: 'String',
    validate: V.isCustomerLabelName,
  },
  BitcoinAddress: {
    typeOf: 'String',
    validate: V.bitcoin.isAddress,
  },
  EthereumAddress: {
    typeOf: 'String',
    validate: V.ethereum.isAddress,
  },
  EthereumTx: {
    typeOf: 'String',
    validate: V.ethereum.isTxHash,
  },
  EthereumBlockHash: {
    typeOf: 'String',
    validate: V.ethereum.isBlockHash,
  },
  EthereumWeiAmount: {
    typeOf: 'String',
    validate: V.ethereum.isValidWeiAmount,
  },
  EthereumAddressCode: {
    typeOf: 'String',
    validate: V.ethereum.isAddressCode,
  },
  MineId: {
    typeOf: 'String',
    validate(x) {
      return x === 'mine';
    },
  },
  BitcoinTxHash: {
    typeOf: 'String',
    validate: V.bitcoin.isTxHash,
  },
  BitcoinAddressArray: {
    typeOf: 'Array',
    validate(as) {
      return _.every(as, V.bitcoin.isAddress);
    },
  },
  BitcoinTxHashArray: {
    typeOf: 'Array',
    validate(as) {
      return _.every(as, V.bitcoin.isTxHash);
    },
  },
  BitcoinTxHex: {
    typeOf: 'String',
    validate: V.bitcoin.isTxHex,
  },
  BitcoinScriptHex: {
    typeOf: 'String',
    validate: V.bitcoin.isScriptHex,
  },
  BitcoinHDPath: {
    typeOf: 'String',
    validate: V.bitcoin.isHDPath,
  },
  BitcoinPublicKey: {
    typeOf: 'String',
    validate: V.bitcoin.isPublicKey,
  },
  BitcoinXpub: {
    typeOf: 'String',
    validate: V.bitcoin.isHDPublicKey,
  },
  BitcoinTxSignature: {
    typeOf: 'String',
    validate: V.bitcoin.isTxSignature,
  },
  BitcoinBlockHeight: {
    typeOf: 'Number',
    validate: V.bitcoin.isBlockHeight,
  },
};

// add support for nullable properties
const withNullable = Object.keys(customTypes).reduce((acc, _type) => {
  acc[_type] = customTypes[_type];
  const { typeOf, validate } = customTypes[_type];

  // for every type, add Nullable+type. A nullable string can also be empty
  acc[`Nullable${_type}`] = Object.assign({}, customTypes[_type], {
    validate: (x) => x == null || (typeOf === 'String' && x === '') || validate(x),
  });

  return acc;
}, {});

module.exports = {
  parseType(str) {
    return TC.parseType(str);
  },
  parsedTypeCheck(type, obj) {
    return TC.parsedTypeCheck(type, obj, this.opts);
  },
  typeCheck(type, obj) {
    return TC.typeCheck(type, obj, this.opts);
  },
  opts: {
    customTypes: withNullable,
  },
  ensureType(type, obj, msg) {
    return V.ensure(this.typeCheck(type, obj), msg);
  },
  checkArg(obj, prop, type, msg) {
    let message;
    if (msg == null) {
      message = `Invalid ${prop}`;
    } else {
      message = msg;
    }
    this.ensureType(type, obj[prop], message);
    return obj[prop];
  },
  checkArgs(input, types) {
    const inputKeys = {};
    const [typ] = types;
    const fields = typ.of;
    let numInputKeys = 0;
    let numKeys;

    Object.keys(input).forEach((k) => {
      inputKeys[k] = true;
      numInputKeys += 1;
    });

    numKeys = 0;

    Object.keys(fields).forEach((key) => {
      const objTypes = fields[key];
      V.ensure(this.parsedTypeCheck(objTypes, input[key]), `Invalid ${key}`);
      if (inputKeys[key]) {
        numKeys += 1;
      }
    });

    if (!(typ.subset || numInputKeys === numKeys)) {
      throw new E.BadRequest('invalid extra arguments are present');
    }
  },
};
