const V = require('validator');
const _ = require('lodash');
const {
  crypto: { Signature },
  HDPrivateKey,
  HDPublicKey,
  PublicKey,
  Script,
  Transaction,
} = require('bitcore-lib');
const addressValidator = require('wallet-address-validator');
const web3 = require('web3-utils');
const E = require('./errors');

const validations = {
  _validate(Err, pred, x, msg) {
    if (pred(x)) {
      return x;
    }
    throw new Err(msg);
  },
  _validateArg(pred, x, msg) {
    return this._validate(E.InvalidArguments, pred, x, msg);
  },
  _tryCheck(fn, arg) {
    try {
      fn(arg);
      return true;
    } catch (error) {
      return false;
    }
  },

  exists(x) {
    return x != null;
  },
  nonEmpty(x) {
    return (x != null ? x.length : undefined) > 0;
  },
  isNonEmptyString(x) {
    return _.isString(x) && (x != null ? x.length : undefined) > 0;
  },
  ensureShortEnough(limit, x) {
    const l = x != null ? x.length : undefined;
    if (l <= limit) {
      return true;
    }
    throw new E.BadRequest(`Max query size exceeded: ${l} / ${limit}`);
  },
  ensure(crit, msg) {
    if (crit) {
      return true;
    }
    throw new E.BadRequest(msg);
  },
  argExists(x, msg) {
    return this._validateArg(this.exists, x, msg);
  },

  check: {
    matches(required, given, msg) {
      const newGiven = _.uniq(given);
      const crit = _.intersection(newGiven, required).length === given.length;
      if (crit) {
        return true;
      }
      throw new E.BadRequest(msg);
    },
    wasFound(type) {
      return (item) => {
        if (item == null) {
          throw new E.NotFound(`Unknown ${type}`);
        } else {
          return item;
        }
      };
    },
    wasCreated(msg) {
      return (arr) => {
        if (!arr[1]) {
          throw new E.BadRequest(msg);
        } else {
          return arr;
        }
      };
    },
    isTrue(crit, msg) {
      if (crit) {
        return true;
      }
      throw new E.BadRequest(msg);
    },
    isFalse(crit, msg) {
      if (!crit) {
        return true;
      }
      throw new E.BadRequest(msg);
    },
  },

  isCustomerReference(x) {
    return (
      _.isString(x)
      && (x != null ? x.length : undefined) > 0
      && (x != null ? x.length : undefined) <= 100
    );
  },
  isCustomerLabelName(x) {
    return (
      _.isString(x)
      && (x != null ? x.length : undefined) > 0
      && (x != null ? x.length : undefined) <= 50
    );
  },
  bitcoin: {
    isAddress(str) {
      const network = process.env.BITCOIN_NETWORK === 'testnet' ? 'testnet' : 'prod';
      return typeof str === 'string' && addressValidator.validate(str.trim(), 'BTC', network);
    },
    isBech32Address(str) {
      return /^bc1[ac-hj-np-z02-9]{6,86}|^BC1[AC-HJ-NP-Z02-9]{6,86}/.test(str);
    },
    isHDPublicKey(str) {
      try {
        HDPublicKey(str);
        return true;
      } catch (error) {
        return false;
      }
    },
    isPublicKey(str) {
      try {
        PublicKey(str);
        return true;
      } catch (error) {
        return false;
      }
    },
    isTxHash(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      return str.length === 64;
    },
    isTxHex(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      try {
        Transaction(str);
        return true;
      } catch (error) {
        return false;
      }
    },
    isHDPath(str) {
      return HDPrivateKey.isValidPath(str);
    },
    isScriptHex(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      try {
        Script(str);
        return true;
      } catch (error) {
        return false;
      }
    },
    isTxSignature(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      try {
        const b = Buffer.from(str, 'hex');
        Signature.fromTxFormat(b);
        return true;
      } catch (error) {
        return false;
      }
    },
    isBlockHeight(obj) {
      return _.isInteger(+obj) && parseInt(obj, 10) >= 0;
    },
  },
  bitcoinCash: {
    isAddress(str) {
      return /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$|^(bitcoincash:)?[q|p][a-z0-9]{41}$|^(BITCOINCASH:)?[Q|P][A-Z0-9]{41}$/.test(str);
    },
    isTxHash(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      return str.length === 64;
    },
  },
  ethereum: {
    isAddress(str) {
      return web3.isAddress(str);
    },
    isBlockHash(str) {
      return str.length === 66 && web3.isHexStrict(str);
    },
    isTxHash(str) {
      return str.length === 66 && web3.isHexStrict(str);
    },
    isAddressCode(str) {
      return web3.isHexStrict(str);
    },
    isValidWeiAmount(str) {
      try {
        web3.fromWei(str);
        return true;
      } catch (e) {
        return false;
      }
    },
  },
  litecoin: {
    isAddress(str) {
      return /^[3LM][a-km-zA-HJ-NP-Z1-9]{24,33}$|^ltc1[ac-hj-np-z02-9]{6,86}$|^LTC1[AC-HJ-NP-Z02-9]{6,86}$/.test(str);
    },
    isTxHash(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      return str.length === 64;
    },
  },
  ripple: {
    isAddress(str) {
      return addressValidator.validate(str, 'XRP');
    },
    isTxHash(str) {
      if (!V.isHexadecimal(str)) {
        return false;
      }
      return str.length === 64;
    },
  },
};

Object.keys(V).forEach((k) => {
  const v = V[k];
  // don't trigger validation with null values (prevents swagger 500 error)
  validations[k] = (x) => (x === null ? false : v(x));
});

module.exports = validations;
