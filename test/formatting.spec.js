const chai = require('chai');
const formatting = require('../lib/formatting');

const { expect } = chai;

(function formattingTest() {
  const { sqlEscapeWildcard, rethrowError } = formatting;

  describe('formatting.sqlEscapeWildcard', () => {
    describe('when passed an empty string', () => {
      it('returns an empty string', () => sqlEscapeWildcard('').should.equal(''));
    });
    describe('when passed a string with no sql wildcards', () => {
      it('returns the same string', () => {
        const str = 'foo bar';
        return sqlEscapeWildcard(str).should.equal(str);
      });
    });
    return describe('when passed a string with sql wildcards', () => {
      it('returns the string with \\ % _ escaped', () => {
        const str = 'foo\\dfs_erwr%rewr';
        return sqlEscapeWildcard(str).should.equal('foo\\\\dfs\\_erwr\\%rewr');
      });
    });
  });

  describe('rethrowError', () => {
    it('returns an error when given a string', () => rethrowError('myFunc', { error: 'Oh dear, myFunc is broken' }).should.equal('myFunc - undefined: undefined - Oh dear, myFunc is broken'));

    it('returns an error when passed an error object', () => rethrowError('myFunc', {
      name: "I'm a teapot",
      statusCode: 418,
      error: "I'm a little teapot, short and stout",
    }).should.equal("myFunc - I'm a teapot: 418 - I'm a little teapot, short and stout"));

    it('accepts an array containing an error', () => {
      const thrownError = rethrowError('myFunc', {
        error: ["I'm a teapot", 418, "I'm a little teapot, short and stout"],
      });

      const expected = JSON.stringify('myFunc - undefined: undefined - \n[\n  "I\'m a teapot",\n  418,\n  "I\'m a little teapot, short and stout"\n]')

      expect(JSON.stringify(thrownError)).to.equal(expected);
    });
  });
}).call(this);
