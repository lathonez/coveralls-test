const chai = require('chai');
const validations = require('../lib/validations');

const { expect, assert } = chai;

describe('Validations', () => {
  describe('global checks', () => {
    describe('exists', () => {
      it('should return true when passed any value', () => validations.exists('x').should.be.true);

      it('should return false when not passed a value', () => validations.exists().should.be.false);
    });

    describe('nonEmpty', () => {
      it('should return true when passed any value with a length', () => validations.nonEmpty('x').should.be.true
        && validations.nonEmpty(['x']));

      it('should return false when passed a value without a length', () => validations.nonEmpty().should.be.false
        && validations.nonEmpty({}).should.be.false
        && validations.nonEmpty(null).should.be.false
        && validations.nonEmpty(undefined).should.be.false);
    });

    describe('isNonEmptyString', () => {
      it('should return true when passed a string of any length', () => validations.isNonEmptyString('x').should.be.true
        && validations.isNonEmptyString('Do not go gentle into that good night, Old age should burn and rage at close of day; Rage, rage against the dying of the light.').should.be.true);

      it('should return false when passed an empty string or something that is not a string', () => validations.isNonEmptyString('').should.be.false
        && validations.isNonEmptyString(['x']).should.be.false
        && validations.isNonEmptyString({ key: 'val' }).should.be.false);
    });

    describe('ensureShortEnough', () => {
      it('should return true when the given string is shorter in length than the given length', () => validations.ensureShortEnough(3, 'foo').should.be.true
        && validations.ensureShortEnough(3, 'fo').should.be.true);

      it('should throw a BadRequest error if the given string is longer than the given length', () => {
        expect(() => validations.ensureShortEnough(3, 'foo!')).to.throw('Max query size exceeded: 4 / 3');
      });

      it('should throw a BadRequest error if the given string is null or undefined', () => {
        expect(() => validations.ensureShortEnough(3, null)).to.throw('Max query size exceeded: undefined / 3');
        expect(() => validations.ensureShortEnough(3, undefined)).to.throw('Max query size exceeded: undefined / 3');
      });
    });

    describe('ensure', () => {
      it('should return true if the first argument is not null or undefined', () => validations.ensure('thing', 'Error message').should.be.true
        && validations.ensure(['array'], 'Error message').should.be.true
        && validations.ensure({ key: 'val' }).should.be.true);
      it('should throw a BadRequest error if the first argument is null or undefined', () => {
        expect(() => validations.ensure(null, 'Error message').to.throw('Error message'));
        expect(() => validations.ensure(undefined, 'Error message').to.throw('Error message'));
      });
    });
  });

  describe('check', () => {
    describe('matches', () => {
      it('should return true when passed two arrays that exactly match', () => validations.check.matches([1, 2, 3, 4, 5], [1, 2, 3, 4, 5], 'error message').should.be.true);

      it('should throw an error when passed two arrays that do not exactly match', () => {
        expect(() => validations.check.matches([1, 2, 3, 4], [1, 2, 3, 4, 5], 'Error message').to.throw('Error message'));
      });
    });

    describe('wasFound', () => {
      it('should return the string passed if this is not null', () => {
        expect(validations.check.wasFound()('string')).to.equal('string');
      });
      it('should throw an error if the item is null', () => {
        expect(() => validations.check.wasFound('no value')()).to.throw('Unknown no value');
      });
    });

    describe('wasCreated', () => {
      it('should return the array passed if it is not empty', () => {
        expect(validations.check.wasCreated()([1, 2, 3])).to.eql([1, 2, 3]);
      });

      it('should throw an error if the array is empty', () => {
        expect(() => validations.check.wasCreated('Oh no! No array!')([])).to.throw('Oh no! No array!');
      });
    });

    describe('isTrue', () => {
      const count = 2;

      it('should return true when the first arg is truthy', () => {
        assert(validations.check.isTrue(count === 2, 'Count equals 2'), 'isTrue should be false');
      });

      it('should throw an error when the first arg is falsy', () => {
        expect(() => validations.check.isTrue(count <= 1, 'Count is greater than 1')).to.throw('Count is greater than 1');
      });
    });

    describe('isFalse', () => {
      const count = 2;

      it('should return true when the first arg is falsy', () => {
        assert(validations.check.isFalse(count === 3, 'Count should not equal 3'), 'isFalse should be true');
      });

      it('should throw an error when the first arg is truthy', () => {
        expect(() => validations.check.isFalse(count > 1, 'Count should be greater than 1')).to.throw('Count should be greater than 1');
      });
    });

    describe('isCustomerReference', () => {
      it('should return true if the string is longer than 0 and less than or equal to 100 characters', () => {
        assert(validations.isCustomerReference('1'), 'isCustomerReference fails when the string length is 1');
        assert(validations.isCustomerReference('Spots of sunshine lie on the surface of the water dance, dance, and their reflections wobble.'), 'isCustomerReference fails when the string length is 100');
      });

      it('should return false when passed an empty string or one longer than 100 characters, or a non-string', () => {
        assert(!validations.isCustomerReference(''), 'isCustomerReference passes when passed an empty string');
        assert(!validations.isCustomerReference('Little spots of sunshine lie on the surface of the water and dance, dance, and their reflections wobble deliciously over the ceiling;'), 'isCustomerReference passes with string over 100 characters');
        assert(!validations.isCustomerReference(['array']), 'isCustomerReference passes when passed a non-string');
      });
    });

    describe('isCustomerLabelName', () => {
      it('should return true if the string is longer than 0 and less than or equal to 50 characters', () => {
        assert(validations.isCustomerLabelName('1'), 'isCustomerLabelName fails when the string length is 1');
        assert(validations.isCustomerLabelName('William Shakespeare was poet, playwright and actor'), 'isCustomerLabelName fails when the string length is 50');
      });

      it('should return false when passed an empty string or one longer than 50 characters, or a non-string', () => {
        assert(!validations.isCustomerLabelName(''), 'isCustomerLabelName passes when passed an empty string');
        assert(!validations.isCustomerLabelName("Many of Shakespeare's plays were published in editions of varying quality and accuracy in his lifetime."), 'isCustomerLabelName passes with string over 50 characters');
        assert(!validations.isCustomerLabelName(['array']), 'isCustomerLabelName passes when passed a non-string');
      });
    });
  });

  describe('BTC', () => {
    describe('isAddress', () => {
      it('should correctly validate a valid BTC address', () => validations.bitcoin.isAddress('1LFnX5KT4CMKud7KB9xaXFZFkD3H8bUD16').should.be.true);

      it('should correctly validate an incorrect BTC address', () => validations.bitcoin.isAddress('1LFnX5KT4CMKud7KB9xaXFZFkD3H8b').should.be.false);
    });

    describe('isBech32Address', () => {
      it('should correctly validate a Bech32 address', () => validations.bitcoin.isBech32Address('bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq').should.be.true);
      it('should correctly validate an invalide Bech32 address', () => validations.bitcoin.isBech32Address('4ef47f6eb681d5d9fa2f7e16336cd629303c635e8da51e425b76088be9c8744c').should.be.false);
    });

    describe('isHDPublicKey', () => {
      it('should return true when passed an HD public key', () => validations.bitcoin.isHDPublicKey('xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi').should.be.true);
      it('should return false when not passed an HD public key', () => validations.bitcoin.isHDPublicKey('bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq').should.be.false);
    });

    describe('isPublicKey', () => {
      it('should return true when passed a public key', () => validations.bitcoin.isPublicKey('046c04c02f1138f440e8c5e9099db938bfba93d0389528bb7f6bf423ae203a2edcfba133f0409023d7ea13ac01c5aeedaf0bbfbeb8b82e9b48410d93a296da5b0c').should.be.true);
      it('should return false when not passed a public key', () => validations.bitcoin.isPublicKey('1EHNa6Q4Jz2uvNExL497mE43ikXhwF6kZm').should.be.false);
    });

    describe('isTxHash', () => {
      it('should correctly validate a valid BTC hash', () => validations.bitcoin.isTxHash('835346f3ea00a23df60ca5dc24afce67810c792ac4505e544410d4b19e7c95a0').should.be.true);
      it('should correctly validate an incorrect BTC hash', () => validations.bitcoin.isTxHash('835346f3ea00a23df60caafce67810c792ac4505e544410d4b19e7c95a0').should.be.false);
      it('should return false if the string passed is not a hex string', () => validations.bitcoin.isTxHex('Definitely not a hex string').should.be.false);
    });

    describe('isTxHex', () => {
      const hex = '0100000002d8c8df6a6fdd2addaf589a83d860f18b44872d13ee6ec3526b2b470d42a96d4d000000008b483045022100b31557e47191936cb14e013fb421b1860b5e4fd5d2bc5ec1938f4ffb1651dc8902202661c2920771fd29dd91cd4100cefb971269836da4914d970d333861819265ba014104c54f8ea9507f31a05ae325616e3024bd9878cb0a5dff780444002d731577be4e2e69c663ff2da922902a4454841aa1754c1b6292ad7d317150308d8cce0ad7abffffffff2ab3fa4f68a512266134085d3260b94d3b6cfd351450cff021c045a69ba120b2000000008b4830450220230110bc99ef311f1f8bda9d0d968bfe5dfa4af171adbef9ef71678d658823bf022100f956d4fcfa0995a578d84e7e913f9bb1cf5b5be1440bcede07bce9cd5b38115d014104c6ec27cffce0823c3fecb162dbd576c88dd7cda0b7b32b0961188a392b488c94ca174d833ee6a9b71c0996620ae71e799fc7c77901db147fa7d97732e49c8226ffffffff02c0175302000000001976a914a3d89c53bb956f08917b44d113c6b2bcbe0c29b788acc01c3d09000000001976a91408338e1d5e26db3fce21b011795b1c3c8a5a5d0788ac00000000';
      it('should return true if passed a hex string Tx hash', () => validations.bitcoin.isTxHex(hex).should.be.true);
      it('should return false if the string passed is not a hex string', () => validations.bitcoin.isTxHex('definitelyNotAHexString').should.be.false);
      it('should return false if the string passed is a hex string, but is not a tx hash', () => validations.bitcoin.isTxHex('4ef47f6eb681d5d9fa2f7e16336cd629303c635e8da51e425b76088be9c8744c').should.be.false);
    });

    describe('isHDPath', () => {
      it('should return true when passed a valid HDPrivateKey path', () => validations.bitcoin.isHDPath('xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi'));
      it('should return false when passed a string that is not a valid HDPrivateKey path', () => validations.bitcoin.isHDPath('bc1qar0srrr7xfkvy5l643lydnw9re59gtzzwf5mdq').should.be.false);
    });

    describe('isScriptHex', () => {
      it('should return true when passed a script hex', () => validations.bitcoin.isScriptHex('01000000011e2e8cfdf21d68f90974cd557a30c11894cc8cfec5cd5330e41846c2c84566bd000000006b4830450221009f61f453f44e807fdc538ca21710393d34005dd5709dabd8b6b9ccf09ea0b36a0220420d91a33f43d7b979471220e12cd1025975bd2e6c0bf2a14eb65ad89d578104012102de8f92034b9b3c956c1896d23a628537561e29faa772438aac2265c91ede6519ffffffff0118566202000000001976a914b27f04e510293c3e530e7b7bdf71f59118030e1e88ac00000000').should.be.true);
      it('should return false if the string passed is not a hex string', () => validations.bitcoin.isScriptHex('definitelyNotAHexString').should.be.false);
      it('should return false if the string passed is a hex string, but is not a script hex', () => validations.bitcoin.isScriptHex('mpXwg4jMtRhuSpVq4xS3HFHmCmWp9NyGKt').should.be.false);
    });

    describe('isTxSignature', () => {
      it('should return true when passed a tx hash that includes a signature', () => validations.bitcoin.isTxSignature('304502206e21798a42fae0e854281abd38bacd1aeed3ee3738d9e1446618c4571d1090db022100e2ac980643b0b82c0e88ffdfec6b64e3e6ba35e7ba5fdd7d5d6cc8d25c6b241501').should.be.true);
      it('should return false when passed a tx hash that does not include a signature', () => validations.bitcoin.isTxSignature('f5d8ee39a430901c91a5917b9f2dc19d6d1a0e9cea205b009ca73dd04470b9a6').should.be.false);
      it('should return false if the string passed is not a hex string', () => validations.bitcoin.isTxSignature('definitelyNotAHexString').should.be.false);
    });

    describe('isBlockHeight', () => {
      it('should return true when passed a value greater than or equal to 0', () => validations.bitcoin.isBlockHeight(986789).should.be.true
        && validations.bitcoin.isBlockHeight('987654').should.be.true);
      it('should return false when passed a value that is not greater than or equal to 0', () => validations.bitcoin.isBlockHeight(null).should.be.false
        && validations.bitcoin.isBlockHeight('string').should.be.false);
    });
  });

  describe('BCH', () => {
    describe('isAddress', () => {
      it('should correctly validate valid BCH addresses', () => validations.bitcoinCash.isAddress('bitcoincash:qrrfmt57avxfd2476gqxqq54djtcvjuzjc2qtsrzcw').should.be.true
        && validations.bitcoinCash.isAddress('qqrxa0h9jqnc7v4wmj9ysetsp3y7w9l36u8gnnjulq').should.be.true);

      it('should correctly validate an incorrect BCH address', () => validations.bitcoinCash.isAddress('qqrxa0h9jqnc7v4wmj3y7w9l36u8gnnjulq').should.be.false);
    });

    describe('isTxHash', () => {
      it('should correctly validate a valid BCH hash', () => validations.bitcoinCash.isTxHash('a32c4d2f9f2e322f19867dbacb7609a1be63c914b8f41775082b50d3ee1a0ce6').should.be.true);

      it('should correctly validate an incorrect BCH hash', () => validations.bitcoinCash.isTxHash('cd1bdc201022ca9386d8757b1c49ecc6ee3769cad2d42ab6e7f9e7bd831').should.be.false);

      it('should return false when passed a non-hex string', () => validations.bitcoinCash.isTxHash('Not a hex string!').should.be.false);
    });
  });

  describe('ETH', () => {
    describe('isAddress', () => {
      it('should return true if the address evaluates as a web3 address', () => validations.ethereum.isAddress('0xa6Bc4b3fe7F2756124d30D896Fe4457711e9d214').should.be.true);
      it('should return false if the address does not evaluate to a web3 address', () => validations.ethereum.isAddress('Bc4b3fe7F2756124d30D896Fe4457711e9d214').should.be.false);
    });

    describe('isBlockHash', () => {
      it('should return true when given a valid hash', () => validations.ethereum.isBlockHash('0xec6491abd46a1a9a71b78f4b84d96fd49525aa4f66c8d8c3588c489f2f88eb0e').should.be.true);
      it('should return false when given an vinalid hash', () => validations.ethereum.isBlockHash('0xec6491abd46a1a9a71b78f4b84d96fd49525aa4f66c8d8c3588c489eb0e').should.be.false);
    });

    describe('isTxHash', () => {
      it('should correctly validate a valid ETH hash', () => validations.ethereum.isTxHash('0xec6491abd46a1a9a71b78f4b84d96fd49525aa4f66c8d8c3588c489f2f88eb0e').should.be.true);

      it('should correctly validate an incorrect ETH hash', () => validations.ethereum.isTxHash('0xec6491abd46a1a9a71b7896fd49525aa4f66c8d8c3588c489f2f88eb0e').should.be.false);
    });

    describe('isAddressCode', () => {
      it('should return true if the string is a hex address', () => validations.ethereum.isAddressCode('0xa6Bc4b3fe7F2756124d30D896Fe4457711e9d214').should.be.true);
      it('should return false if the string is not a hex address', () => validations.ethereum.isAddressCode('0xa6Bc4b3fe7F27yu124d30D896Fe4457711e9d214').should.be.false);
    });

    describe('isValidWeiAmount', () => {
      it('should return true when passed an amount that evaluates to wei', () => validations.ethereum.isValidWeiAmount('7000000000000000').should.be.true);
      it('should return false when passed an amount that is not valid in wei', () => validations.ethereum.isValidWeiAmount(7000000000000000).should.be.false
        && validations.ethereum.isValidWeiAmount('1000.00').should.be.false);
    });
  });

  describe('LTC', () => {
    describe('isAddress', () => {
      it('should correctly validate valid LTC addresses', () => validations.litecoin.isAddress('LMR7przNNMS3GpaD6Ygq64NxbsJsZAqGgM').should.be.true);

      it('should correctly validate an incorrect LTC address', () => validations.litecoin.isAddress('t1t8BzkNb8d98o8kpbwrjtUjMX3GMF').should.be.false);
    });

    describe('isTxHash', () => {
      it('should correctly validate a valid LTC hash', () => validations.litecoin.isTxHash('b0b858b34675cc8f851e6a2174295ada574837422538efe9ca83ef405d2faf29').should.be.true);

      it('should correctly validate an incorrect LTC hash', () => validations.litecoin.isTxHash('b0b858b34675cc8f851e6a2295ada574837422538efe9ca83ef405d2faf29').should.be.false);

      it('should return false when passed a non-hex string', () => validations.litecoin.isTxHash('Not a hex code here').should.be.false);
    });
  });

  describe('XRP', () => {
    describe('isAddress', () => {
      it('should correctly validate valid XRP addresses', () => validations.ripple.isAddress('rDV3RhpWznRCmtpD17RNxg6t4Hg8NPV8mF').should.be.true);

      it('should correctly validate an incorrect XRP address', () => validations.ripple.isAddress('rDV3RhpWznRCmtpD17RNxg0t4Hg8NPV8mF').should.be.false);
    });

    describe('isTxHash', () => {
      it('should correctly validate a valid XRP hash', () => validations.ripple.isTxHash('C6593DD2A32ED5120DF1FE80665B84A9CCAF06ADDC2EF89F720CBAC3E8069C0E').should.be.true);

      it('should correctly validate an incorrect XRP hash', () => validations.ripple.isTxHash('C6593DD2A32ED5120DF1FE84A9CCAF06ADDC2EF89F720CBAC3E8069C0E').should.be.false);

      it('should return false when passed a non-hex string', () => validations.ripple.isTxHash('Yet again this is not a hex string').should.be.false);
    });
  });
});
