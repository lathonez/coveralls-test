{includeNested} = require '../lib/ormHelpers'

describe 'ormIncludeNested', ->
  orm = {a: 'a', b: 'b', c: 'c', d: 'd'}

  describe 'when passed an empty object', ->
    it 'returns an empty array', ->
      includeNested orm, {}
        .should.deep.equal []

  describe 'when passed an object with no inclusions', ->
    it 'returns an array of just the key', ->
      includeNested orm, a: {}
        .should.deep.equal ['a']

  describe 'when passed a hierarchy of inclusions', ->
    it 'should nest them in sequelize style', ->
      includeNested orm, {a: b: {c: {}, d: {}}}
        .should.deep.equal [
          model: 'a'
          include: [
            model: 'b'
            include: ['c', 'd']
          ]
        ]
