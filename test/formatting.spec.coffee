formatting = require '../lib/formatting'

describe 'formatting.sqlEscapeWildcard', ->
  {sqlEscapeWildcard} = formatting

  describe 'when passed an empty string', ->
    it 'returns an empty string', ->
      sqlEscapeWildcard('').should.equal ''

  describe 'when passed a string with no sql wildcards', ->
    it 'returns the same string', ->
      str = 'foo bar'
      sqlEscapeWildcard(str).should.equal str

  describe 'when passed a string with sql wildcards', ->
    it 'returns the string with \\ % _ escaped', ->
      str = "foo\\dfs_erwr%rewr"
      sqlEscapeWildcard(str).should.equal "foo\\\\dfs\\_erwr\\%rewr"
